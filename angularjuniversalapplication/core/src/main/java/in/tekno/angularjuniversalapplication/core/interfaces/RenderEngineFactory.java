package in.tekno.angularjuniversalapplication.core.interfaces;
import in.tekno.angularjuniversalapplication.core.interfaces.RenderEngine;
public interface RenderEngineFactory {
    /**
     * Create a new render engine.
     *
     * @return New render engine
     */
    RenderEngine createRenderEngine();
}

