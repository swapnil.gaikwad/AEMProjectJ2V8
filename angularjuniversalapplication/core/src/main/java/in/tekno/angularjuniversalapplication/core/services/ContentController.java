package in.tekno.angularjuniversalapplication.core.services;


/*Ignore this page 
*/

public class ContentController {

	private final RenderService renderservice;

	public ContentController(RenderService renderservice) {
		this.renderservice = renderservice;
	}

	public String showIndex() throws Exception {
		return renderservice.renderPage("/").get();
	}

	public String showLogin() throws Exception {
		return renderservice.renderPage("/login").get();
	}

	public String showLogout() throws Exception {
		return renderservice.renderPage("/logout").get();
	}

	public String showPage() throws Exception {
		return renderservice.renderPage("/page").get();
	}

	public String showPageHome() throws Exception {
		return renderservice.renderPage("/page/home").get();
	}

	public String showPageAbout() throws Exception {
		return renderservice.renderPage("/page/about").get();
	}
}
