package in.tekno.angularjuniversalapplication.core.services;

import java.io.File;
import java.io.IOException;
import java.io.InputStream;
import java.nio.charset.StandardCharsets;
import java.util.concurrent.Future;

import org.osgi.service.component.annotations.Component;

import in.tekno.angularjuniversalapplication.core.interfaces.RenderEngineFactory;
import in.tekno.angularjuniversalapplication.core.renderer.RenderConfiguration;
import in.tekno.angularjuniversalapplication.core.renderer.RenderUtils;
import in.tekno.angularjuniversalapplication.core.renderer.Renderer;
import in.tekno.angularjuniversalapplication.core.v8renderer.V8RenderEngineFactory;


public class RenderService  {
	
private final Renderer renderer;

public  RenderService() throws IOException {
	
    // Load the template and create a temporary server bundle file from the resource (This file will of course never change until manually edited)
    InputStream templateinputstream = getClass().getResourceAsStream("/public/index.html");
    InputStream serverbundleinputstream = getClass().getResourceAsStream("/server.js");
    
    String templatecontent = RenderUtils.getStringFromInputStream(templateinputstream, StandardCharsets.UTF_8);
    File serverbundlefile = RenderUtils.createTemporaryFileFromInputStream("serverbundle", "tmp", serverbundleinputstream);
    // File localserverbundlefile = new File("<Local server bundle on the file system>"); --> Also enable auto reload in the configuration

    // Create the configuration. For real live reloading, don't use a temporary file but the real generated on from the file system
    RenderConfiguration configuration = new RenderConfiguration(templatecontent, serverbundlefile, 4, false);

 // Create the V8 render engine factory for spawning render engines
    RenderEngineFactory factory = (RenderEngineFactory) new V8RenderEngineFactory();
    
    // Create and start the renderer
    this.renderer = new Renderer(factory, configuration);
    this.renderer.startRenderer();
   }

    public Future<String> renderPage(String uri) {
    // Render a request and return a resolvable future
      return renderer.addRenderRequest(uri);
}


	
}

