package in.tekno.angularjuniversalapplication.core.servlets;

import java.io.IOException;
import java.util.HashMap;
import java.util.Map;
import java.util.concurrent.ExecutionException;

import javax.jcr.Node;
import javax.jcr.Property;
import javax.jcr.RepositoryException;
import javax.servlet.Servlet;
import javax.servlet.ServletException;

import org.apache.sling.api.SlingHttpServletRequest;
import org.apache.sling.api.SlingHttpServletResponse;
import org.apache.sling.api.resource.LoginException;
import org.apache.sling.api.resource.Resource;
import org.apache.sling.api.resource.ResourceResolver;
import org.apache.sling.api.resource.ResourceResolverFactory;
import org.apache.sling.api.servlets.SlingSafeMethodsServlet;
import org.apache.sling.jcr.api.SlingRepository;
import org.osgi.service.component.annotations.Component;
import org.osgi.service.component.annotations.Reference;

import in.tekno.angularjuniversalapplication.core.services.RenderService;


@Component(service = Servlet.class, property = { "sling.servlet.methods=get", "sling.servlet.methods=post",
		"sling.servlet.paths=/bin/ng" })

public class AngularUniversalServlet extends SlingSafeMethodsServlet{
	

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;

	@Reference
	ResourceResolverFactory resourceResolverFactory;
	
	@Reference
	SlingRepository slingRepo;
	
	

	ResourceResolver resolver;

	protected void doGet(SlingHttpServletRequest request, SlingHttpServletResponse response)
			throws ServletException, IOException {
		
		
		
		/*try {
			RenderService renderService = new RenderService();
			renderService.renderPage("/apps/angularjuniversalapplication/angular/public/index.html").get();
			return;
		} catch (InterruptedException | ExecutionException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}*/
		
		
		Map<String, Object> params = new HashMap<>();
		params.put(ResourceResolverFactory.SUBSERVICE,"ngAppSysUser");
		try {
//			Session session = slingRepo.loginService("ngAppSysUser", null);
			
			resolver = resourceResolverFactory.getServiceResourceResolver(params);
			Resource loginPage = resolver.getResource("/apps/angularjuniversalapplication/angular/public/index.html/jcr:content");
			Node loginPageNode = loginPage.adaptTo(Node.class);
			Property loginPageProp = loginPageNode.getProperty("jcr:data");
			
			if(loginPageProp!=null) {
				response.setContentType("text/html");
				String loginPageHtmlStr = loginPageProp.getString();
				
				if(loginPageHtmlStr!=null && !loginPageHtmlStr.isEmpty()) {
					response.setContentLength(loginPageHtmlStr.length());
					response.getWriter().println(loginPageHtmlStr);
				}
				
				
			}
			
		} catch (LoginException | RepositoryException e1) {
			// TODO Auto-generated catch block
			e1.printStackTrace();
		}		

	}

}
