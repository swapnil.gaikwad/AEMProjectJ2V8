package in.tekno.angularjuniversalapplication.core.v8renderer;


import in.tekno.angularjuniversalapplication.core.interfaces.RenderEngine;
import in.tekno.angularjuniversalapplication.core.interfaces.RenderEngineFactory;

public class V8RenderEngineFactory implements RenderEngineFactory {

    /**
     * Create a new V8 render engine.
     *
     * @return New V8 render engine
     */
    @Override
    public RenderEngine createRenderEngine() {
        return new V8RenderEngine();
    }
}

 