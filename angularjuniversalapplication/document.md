# Angular Universal for Java in AEM 6.3

With the introduction of Angular Universal, a solution for dynamically prerendering Angular applications on the server side (SSR) and sending the content directly to the browser as 'already-bootstrapped' application, Angular became more interesting for many developers that were using a Node.js environment. It solved the problems of SEO optimization, empty page previews from Facebooks & others and resource Management.

This project provides a Java API to a local Node.js instance that is responsible for server side rendering the Angular application. For this, the JNI language bindings of the J2V8 projects were used. This makes it possible to use the latest Angular Universal features without having a second [JavaScript] web server (Only a second VM by Node.js) - an integration of a prerendered Angular application into a pure Java stack is now possible.

-----
## Getting started

### Phase 0: Overview

The whole getting started process can be divided into three phases:

 1. Create a new **aem multi module project** and Maven as build system.

 2. Create a new Angular application inside the **ui.apps** of AEM project.

 3. **Integrate** the Angular application into the AEM project.

### Phase 1: Create a new AEM Project

 1. Create a new MultiModule AEM archetype 12 project and remove other project from parent accept core and ui.apps.

 2. Add the below Dependency in AEM ui.apps 6.3

 ```
  <!-- Nodejs and npm plugin -->
			<plugin>
				<groupId>com.github.eirslett</groupId>
				<artifactId>frontend-maven-plugin</artifactId>
				<version>1.6</version>
				<configuration>
					<workingDirectory>src/main/angular</workingDirectory>
				</configuration>
				<executions>
					<execution>
						<id>install node and npm</id>
						<goals>
							<goal>install-node-and-npm</goal>
						</goals>
						<configuration>
							<nodeVersion>v8.9.1</nodeVersion>
							<npmVersion>5.5.1</npmVersion>
						</configuration>
					</execution>
					<execution>
						<id>npm install</id>
						<goals>
							<goal>npm</goal>
						</goals>
						<configuration>
							<arguments>install</arguments>
						</configuration>
					</execution>
					<execution>
						<id>npm run build</id>
						<goals>
							<goal>npm</goal>
						</goals>
						<configuration>
							<arguments>run build</arguments>
						</configuration>
						<phase>generate-resources</phase>
					</execution>
				</executions>
			</plugin>
        </plugins>
 ```
 3. Setup your project and make sure your don't have and error in your project and run cmd 

 ```
 mvn eclipse:eclipse

 ```
### Phase 2: Create a new Angular application

To make the integration with Java as smooth as possible, we will stick to the Maven default layout https://maven.apache.org/guides/introduction/introduction-to-the-standard-directory-layout.html. The Angular application will be located in angularj-universal-example/src/main/angular and the build output.

Install the stable version of **node.js and npm v.8.9.4** application:

```
node -v 

v.8.9.4

npm -v

v.5.4.2
```

Install the Angular CLI and create a new Angular application in ui.apps:

> local and global verion of angular/cli should be similar(1.5.3).

```
npm install -g @angular/cli
ng new angular

```
The above Steps will help you to install and node.js , npm and @angular/cli.


### Phase 3: Integrating Angular Universal into existing CLI Applications.

####  Install Dependencies

Install @angular/platform-server into your project. Make sure you use the same version as the other @angular packages in your project.

> You'll also need ts-loader (for your webpack build we'll show later) and @nguniversal/module-map-ngfactory-loader, as it's used to handle lazy-loading in the context of a server-render. (by loading the chunks right away)

```
npm install --save @angular/platform-server @nguniversal/module-map-ngfactory-loader ts-loader

```
#### Step 1: Prepare your App for Universal rendering

The first thing you need to do is make your AppModule compatible with Universal by adding .withServerTransition() and an application ID to your BrowserModule import:

##### src/app/app.module.ts:

```
@NgModule({
  bootstrap: [AppComponent],
  imports: [
    // Add .withServerTransition() to support Universal rendering.
    // The application ID can be any identifier which is unique on
    // the page.
    BrowserModule.withServerTransition({appId: 'my-app'}),
    ...
  ],

})
export class AppModule {}

```

##### src/app/app.server.module.ts:

> One important thing to Note: We need ModuleMapLoaderModule to help make Lazy-loaded routes possible during Server-side renders with the Angular-CLI.

```
import {NgModule} from '@angular/core';
import {ServerModule} from '@angular/platform-server';
import {ModuleMapLoaderModule} from '@nguniversal/module-map-ngfactory-loader';

import {AppModule} from './app.module';
import {AppComponent} from './app.component';

@NgModule({
  imports: [
    // The AppServerModule should import your AppModule followed
    // by the ServerModule from @angular/platform-server.
    AppModule,
    ServerModule, 
    ModuleMapLoaderModule // <-- *Important* to have lazy-loaded routes work
  ],
  // Since the bootstrapped component is not inherited from your
  // imported AppModule, it needs to be repeated here.
  bootstrap: [AppComponent],
})
export class AppServerModule {}

```

Step 2: Create a server "main" file and tsconfig to build it

Create a main file for your Universal bundle. This file only needs to export your **AppServerModule**. It can go in src. This example calls this file **main.server.ts**:

##### src/main.server.ts

```
export { AppServerModule } from './app/app.server.module';

```
Copy tsconfig.app.json to tsconfig.server.json and change it to build with a "module" target of "commonjs"

##### src/tsconfig.server.json:

```
{
  "extends": "../tsconfig.json",
  "compilerOptions": {
    "outDir": "../out-tsc/app",
    "baseUrl": "./",
    // Set the module format to "commonjs":
    "module": "commonjs",
    "types": []
  },
  "exclude": [
    "test.ts",
    "**/*.spec.ts"
  ],
  // Add "angularCompilerOptions" with the AppServerModule you wrote
  // set as the "entryModule".
  "angularCompilerOptions": {
    "entryModule": "app/app.server.module#AppServerModule"
  }
}

```

#### Step 3: Create a new project in .angular-cli.json

In **.angular-cli.json** there is an array under the key "apps". Copy the configuration for your client application there, and paste it as a new entry in the array, with an additional key "platform" set to "server".

Then, remove the "polyfills" key - those aren't needed on the server, and adjust "main", and "tsconfig" to point to the files you wrote in step 2. Finally, adjust **"outDir"** to a new location (this example uses dist-server).

##### .angular-cli.json:

```
{
  ...
  "apps": [
    {
      // Keep your original application config intact here, this is app 0
      // -EXCEPT- for outDir, update it to dist/browser
      "outDir": "../angular/public" //By Default it's dist/bowser <-- update this
    },
    {
      // This is your server app. It is app 1.
      "platform": "server",
      "root": "src",
      // Build to dist/server instead of dist. This prevents
      // client and server builds from overwriting each other.
      "outDir": "dist/server",
      "assets": [
        "assets",
        "favicon.ico"
      ],
      "index": "index.html",
      // Change the main file to point to your server main.
      "main": "main.server.ts",
      // Remove polyfills.
      // "polyfills": "polyfills.ts",
      "test": "test.ts",
      // Change the tsconfig to point to your server config.
      "tsconfig": "tsconfig.server.json",
      "testTsconfig": "tsconfig.spec.json",
      "prefix": "app",
      "styles": [
        "styles.css"
      ],
      "scripts": [],
      "environmentSource": "environments/environment.ts",
      "environments": {
        "dev": "environments/environment.ts",
        "prod": "environments/environment.prod.ts"
      }
    }
  ],
  ...
}
```

### Building the bundle

With these steps complete, you should be able to **build a server bundle** for your application, using the **--app flag** to tell the CLI to build the server bundle, referencing its index of 1 in the "apps" array in ** .angular-cli.json**:

src/angular use following command

```
# This builds the client application in resources/public/
$ ng build --prod
...
# This builds the server bundle in dist/server/
$ ng build --prod --app 1 --output-hashing=false

# outputs:
Date: 2017-07-24T22:42:09.739Z
Hash: 9cac7d8e9434007fd8da
Time: 4933ms
chunk {0} main.bundle.js (main) 9.49 kB [entry] [rendered]
chunk {1} styles.bundle.css (styles) 0 bytes [entry] [rendered]

```

### Step 4: Setup a webpack config to handle this Node server.ts file and serve your application!

Create a file named webpack.server.config.js at the ROOT of your application.

> This file basically takes that server.ts file, and takes it and compiles it and every dependency it has into dist/server.js.

##### ./webpack.server.config.js (root project level)

```
const path = require('path');
const webpack = require('webpack');

module.exports = {
  entry: {server: './library/server.ts'},
  resolve: { extensions: ['.js', '.ts'] },
  target: 'node',
  // this makes sure we include node_modules and other 3rd party libraries
  externals: [/(node_modules|main\..*\.js)/],
  output: {
    path: path.join(__dirname, '../resources'),
    filename: '[name].js'
  },
  module: {
    rules: [
      { test: /\.ts$/, loader: 'ts-loader' }
    ]
  },
  plugins: [
    // Temporary Fix for issue: https://github.com/angular/angular/issues/11580
    // for "WARNING Critical dependency: the request of a dependency is an expression"
    new webpack.ContextReplacementPlugin(
      /(.+)?angular(\\|\/)core(.+)?/,
      path.join(__dirname, 'src'), // location of your src
      {} // a map of your routes
    ),
    new webpack.ContextReplacementPlugin(
      /(.+)?express(\\|\/)(.+)?/,
      path.join(__dirname, 'src'),
      {}
    )
  ]
}
```

##### library/renderadapter.ts

```
require('zone.js/dist/zone-node');

import {renderModuleFactory} from "@angular/platform-server";

export type RenderCallback = (uuid: string, html: string, error: any) => void;

export class RenderAdapter {

    private appservermodulengfactory: any;

    private callback: RenderCallback;

    private html: string;

    constructor(appservermodulengfactory: any, callback: RenderCallback) {
        this.appservermodulengfactory = appservermodulengfactory;
        this.callback = callback;
        this.html = "<app-root></app-root>";
    }

    setHtml(html: string) {
        this.html = html;
    }

    renderPage(uuid: string, uri: string) {
        renderModuleFactory(this.appservermodulengfactory, {document: this.html, url: uri}).then(html => {
            this.callback(uuid, html, null);
        });
    }
}

```

The create another file **library/server.ts** that will consume the API:

```
import {RenderAdapter, RenderCallback} from "./renderadapter";

const AppServerModuleNgFactory = require('./../dist/main.bundle').AppServerModuleNgFactory;

export declare function registerRenderAdapter(renderadapter: RenderAdapter): void;

export declare function receiveRenderedPage(uuid: string, html: string, error: any): void;

export const rendercallback: RenderCallback = (uuid: string, html: string, error: any) => {
receiveRenderedPage(uuid, html, error);
};

const renderadapter = new RenderAdapter(AppServerModuleNgFactory, rendercallback);
registerRenderAdapter(renderadapter);

```

This file will create a new render adapter and register this adapter via registerRenderAdapter - a method that doesn't exist and will be provided by the Java virtual machine - this is the real glue code. In addition, there is another method receiveRenderedPage that will be also be provided by the Java virtual machine and receives all finished render requests.

Now let's update our Node scripts for building all three applications (Snipped from package.json:

```
"scripts": {
    "start": "npm run build",
    "build": "ng build --app 0 --prod --build-optimizer && ng build --app 1 --prod --output-hashing none && webpack --config webpack.server.config.js"
},

```

Now you can go and run the SPA on your browser.


## Phase 3: Integrate the Angular application into the Java project

Now we switch back to the Java part and create a web application with a controller and a service that is going to render our application.
Create a new Java class file with the following content bellow:

**packages** 

-----------
-  **in.tekno.angularjuniversalapplication.core.services** (Contains RenderSevice and contentController.Here pages are mapped and java class is written)
- **in.tekno.angularjuniversalapplication.core.v8renderer**(contains v8 Engine Render java file)
- **in.tekno.angularjuniversalapplication.core.interfaces** (contains render Interface)
- **in.tekno.angularjuniversalapplication.core.renderer** (Conatins Render java file)


Note 1: In this example we only provide the / route, but the example in angularj-universal-application has several pages and hence routes (Login, Home etc.)

Note 2: Check out the to avoid the manual routes (In the integration you specify them in a property file)

```
package in.tekno.angularjuniversalapplication.core.services;

import java.io.File;
import java.io.IOException;
import java.io.InputStream;
import java.nio.charset.StandardCharsets;
import java.util.concurrent.Future;

import org.osgi.service.component.annotations.Component;

import in.tekno.angularjuniversalapplication.core.renderer.RenderConfiguration;
import in.tekno.angularjuniversalapplication.core.renderer.RenderUtils;
import in.tekno.angularjuniversalapplication.core.interfaces.RenderEngineFactory;
import in.tekno.angularjuniversalapplication.core.renderer.Renderer;
import in.tekno.angularjuniversalapplication.core.v8renderer.V8RenderEngineFactory;

public class RenderService  {
	
private final Renderer renderer;

public  RenderService() throws IOException {
	
    // Load the template and create a temporary server bundle file from the resource (This file will of course never change until manually edited)
    InputStream templateinputstream = getClass().getResourceAsStream("/public/index.html");
    InputStream serverbundleinputstream = getClass().getResourceAsStream("/server.js");
    
    String templatecontent = RenderUtils.getStringFromInputStream(templateinputstream, StandardCharsets.UTF_8);
    File serverbundlefile = RenderUtils.createTemporaryFileFromInputStream("serverbundle", "tmp", serverbundleinputstream);
    // File localserverbundlefile = new File("<Local server bundle on the file system>"); --> Also enable auto reload in the configuration

    // Create the configuration. For real live reloading, don't use a temporary file but the real generated on from the file system
    RenderConfiguration configuration = new RenderConfiguration(templatecontent, serverbundlefile, 4, false);

 // Create the V8 render engine factory for spawning render engines
    RenderEngineFactory factory = (RenderEngineFactory) new V8RenderEngineFactory();
    
    // Create and start the renderer
    this.renderer = new Renderer(factory, configuration);
    this.renderer.startRenderer();
   }

    Future<String> renderPage(String uri) {
    // Render a request and return a resolvable future
      return renderer.addRenderRequest(uri);
}


	
}

```
##### Add Dependency in core in pom.xml

```
    <dependency>
			<groupId>com.eclipsesource.j2v8</groupId>
			<artifactId>j2v8_win32_x86_64</artifactId>
		</dependency> 

```

##### Add profile in pom.xml of parent aem application  

```
<profile>
			<id>windows</id>
			<activation>
				<os>
					<family>windows</family>
				</os>
			</activation>
			<dependencies>
				<dependency>
					<groupId>com.eclipsesource.j2v8</groupId>
					<artifactId>j2v8_win32_x86_64</artifactId>
					<version>4.6.0</version>
				</dependency>
			</dependencies>
		</profile>

	</profiles>
```

> If you find any error in osgi console upload J2V8 jar in osgi console.(https://mvnrepository.com/artifact/com.eclipsesource.j2v8/j2v8_win32_x86_64/4.6.0)
--------
## Creating AEM template and components


### Step 1:

- when we run **npm run build** command in **/angular/public  .js file and index.html** will be created.So, we will transfer all the file in clientlibs.

 ![](images/clientlibs.JPG)
```
<jcr:root xmlns:cq="http://www.day.com/jcr/cq/1.0" xmlns:jcr="http://www.jcp.org/jcr/1.0"
    jcr:primaryType="cq:ClientLibraryFolder"
    categories="angular.bundle"/>
```

### Step 2:

* Install **html-webpack-plugin** (This is a webpack plugin that simplifies creation of HTML files to serve your webpack bundles. This is especially useful for webpack bundles that include a hash in the filename which changes every compilation. You can either let the plugin generate an HTML file for you, supply your own template using lodash templates or use your own loader.)

```
  npm install html-webpack-plugin --save-dev

```
- **webpack.server.config.js**

```
var HtmlWebpackPlugin = require('html-webpack-plugin');
const path = require('path');
const webpack = require('webpack');

module.exports = {
    entry: {  server: './library/server.ts' },
    resolve: { extensions: ['.js', '.ts'] },
    target: 'node',
    // this makes sure we include node_modules and other 3rd party libraries
    externals: [/(node_modules|main\..*\.js)/],
    output: {
        path: path.join(__dirname, '../angular'),
        filename: ''
    },
    module: {
        rules: [
            { test: /\.ts$/, loader: 'ts-loader' }
        ]
    },
    
    plugins: [
    	
    	new HtmlWebpackPlugin({
    		  template: ''
    	}),
        // Temporary Fix for issue: https://github.com/angular/angular/issues/11580
        // for "WARNING Critical dependency: the request of a dependency is an expression"
        new webpack.ContextReplacementPlugin(
            /(.+)?angular(\\|\/)core(.+)?/,
            path.join(__dirname, 'src'), // location of your src
            {} // a map of your routes
        ),
        new webpack.ContextReplacementPlugin(
            /(.+)?express(\\|\/)(.+)?/,
            path.join(__dirname, 'src'),
            {}
        )
    ]
}
```

### Step 3:

- Create template **angulartemplate** 

```
<jcr:root xmlns:sling="http://sling.apache.org/jcr/sling/1.0" xmlns:cq="http://www.day.com/jcr/cq/1.0" xmlns:jcr="http://www.jcp.org/jcr/1.0"
    jcr:description="angulartemplate in angularjapplication"
    jcr:primaryType="cq:Template"
    jcr:title="angulartemplate"
    allowedPaths="[/content(./*)?]"
    ranking="{Long}1">
    <jcr:content
        jcr:primaryType="cq:PageContent"
        sling:resourceType="angularjuniversalapplication/components/structure/angularpage"/>
</jcr:root>
```
- Create Page **angularpage.html**


```
<jcr:root xmlns:sling="http://sling.apache.org/jcr/sling/1.0" xmlns:cq="http://www.day.com/jcr/cq/1.0" xmlns:jcr="http://www.jcp.org/jcr/1.0"
    jcr:description="angularpage of angularjuniversal"
    jcr:primaryType="cq:Component"
    jcr:title="angularpage"
    sling:resourceSuperType="wcm/foundation/components/page"/>

```
```
<!doctype html>
<html lang="en">
<head>
<meta charset="utf-8">
<title>Angular</title>
<base href="http://localhost:4502${properties.path}.html">
<meta name="viewport" content="width=device-width,initial-scale=1">
<link rel="icon" type="image/x-icon" href="favicon.ico">
</head>
<body>


	<app-root></app-root>
	
		<sly data-sly-use.clientLib="/libs/granite/sightly/templates/clientlib.html" data-sly-call="${clientLib.css @ categories='angular.bundle'}"/>
	
	<sly data-sly-use.clientLib="/libs/granite/sightly/templates/clientlib.html" data-sly-call="${clientLib.js @ categories='angular.bundle'}"/>
	

</body>
</html>
```
### Step 4:

- **angular/src/app/components** will contains all pages login,logout,page/about,page/home.So, make sure in **app.routes.ts** we will configure all the path of the routes.


 ![](images/angular.JPG)


In **angularpage.html** it will point to the aem url.

```
<base href="http://localhost:4502${properties.path}.html">

```

- **Now Angular is ready to render pages on Aem Template** 
------


